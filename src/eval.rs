use crate::values::MachineValue;

pub struct EvalContext<'a> {
    parent: Option<&'a EvalContext<'a>>,
    pub args: Vec<MachineValue>,
    stack: Vec<MachineValue>,
}

impl<'a> EvalContext<'a> {
    pub fn new(args: Vec<MachineValue>) -> Self {
        Self {
            parent: None,
            args,
            stack: Vec::new(),
        }
    }

    pub fn allocaof(&mut self, value: MachineValue) -> *mut MachineValue {
        self.stack.push(value);
        self.stack.last_mut().unwrap() as *mut MachineValue
    }

    pub fn call<'c: 'a>(&'c self, args: Vec<MachineValue>) -> EvalContext<'c> {
        Self {
            parent: Some(self),
            args,
            stack: Vec::new(),
        }
    }
}

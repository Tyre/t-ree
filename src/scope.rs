use crate::{
    eval::EvalContext,
    expressions::Expression,
    types::{MachineType, Typed},
    values::MachineValue,
};
use std::collections::HashMap as Map;

fn make_function_type(
    expressions: &Vec<Expression>,
) -> (Box<MachineType>, Vec<MachineType>, Box<Option<MachineType>>) {
    let mut argument_types = Vec::new();
    Expression::query_argument_types_list(expressions, &mut argument_types);
    (
        Box::new(Expression::typify_body(expressions)),
        argument_types
            .into_iter()
            .map(|argument_type| {
                if let Some(ty) = argument_type {
                    ty
                } else {
                    MachineType::Tuple(Vec::new())
                }
            })
            .collect(),
        Box::new(None),
    )
}

pub enum Definition {
    Value(Expression),
    Function(Vec<Expression>),
    Extern(MachineType, Vec<MachineType>, Option<MachineType>),
}

impl Definition {
    pub fn typify(&self) -> MachineType {
        use Definition::*;
        match self {
            Value(expression) => expression.typify(),
            Function(expressions) => {
                let (ret, args, variadic) = make_function_type(expressions);
                MachineType::Function(ret, args, variadic)
            }
            Extern(ret, args, variadic) => MachineType::Function(
                Box::new(ret.clone()),
                args.clone(),
                Box::new(variadic.clone()),
            ),
        }
    }

    pub fn eval(&self, scope: &Scope, context: &mut EvalContext) -> MachineValue {
        match self {
            Definition::Value(expression) => expression.eval(scope, context),
            Definition::Function(expressions) => MachineValue::Function(expressions.clone()),
            Definition::Extern(ret, args, variadic) => {
                MachineValue::Extern(ret.clone(), args.clone(), variadic.clone())
            }
        }
    }

    pub fn call(&self, scope: &Scope, args: Vec<MachineValue>) -> MachineValue {
        let mut context = EvalContext::new(Vec::new());
        self.eval(scope, &mut context)
            .call(scope, context.call(args))
    }
}

pub struct Scope {
    pub types: Map<String, MachineType>,
    pub values: Map<String, Definition>,
    pub libs: Vec<String>,
}

impl Scope {
    pub fn new() -> Scope {
        Scope {
            types: Map::new(),
            values: Map::new(),
            libs: Vec::new(),
        }
    }
}

pub trait Type: PartialEq {
    fn byte_size(&self) -> usize;
}

#[derive(Clone, PartialEq, Debug)]
pub enum IntType {
    I8,
    I16,
    I32,
    I64,
    I128,
    U8,
    U16,
    U32,
    U64,
    U128,
}

impl IntType {
    pub fn signed(size: u8) -> Option<Self> {
        use IntType::*;
        Some(match size {
            8 => I8,
            16 => I16,
            32 => I32,
            64 => I64,
            128 => I128,
            _ => return None,
        })
    }

    pub fn unsigned(size: u8) -> Option<Self> {
        use IntType::*;
        Some(match size {
            8 => U8,
            16 => U16,
            32 => U32,
            64 => U64,
            128 => U128,
            _ => return None,
        })
    }

    pub fn new(size: u8, signed: bool) -> Option<Self> {
        if signed {
            Self::signed(size)
        } else {
            Self::unsigned(size)
        }
    }
}

impl Type for IntType {
    fn byte_size(&self) -> usize {
        use IntType::*;
        match self {
            I8 | U8 => 1,
            I16 | U16 => 2,
            I32 | U32 => 4,
            I64 | U64 => 8,
            I128 | U128 => 16,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum FloatType {
    F32,
    F64,
}

impl FloatType {
    pub fn new(size: u8) -> Option<Self> {
        use FloatType::*;
        Some(match size {
            32 => F32,
            64 => F64,
            _ => return None,
        })
    }
}

impl Type for FloatType {
    fn byte_size(&self) -> usize {
        use FloatType::*;
        match self {
            F32 => 4,
            F64 => 8,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum PrimitiveType {
    Bool,
    Int(IntType),
    Float(FloatType),
}

impl Type for PrimitiveType {
    fn byte_size(&self) -> usize {
        use PrimitiveType::*;
        match self {
            Bool => 1,
            Int(ty) => ty.byte_size(),
            Float(ty) => ty.byte_size(),
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum MachineType {
    Primitive(PrimitiveType),
    Tuple(Vec<MachineType>),
    Array(usize, Box<MachineType>),
    Vector(usize, Box<MachineType>),
    Pointer(Box<MachineType>),
    Function(Box<MachineType>, Vec<MachineType>, Box<Option<MachineType>>),
}

impl Type for MachineType {
    fn byte_size(&self) -> usize {
        use MachineType::*;
        match self {
            Primitive(ty) => ty.byte_size(),
            Tuple(types) => types.into_iter().map(|ty| ty.byte_size()).sum(),
            Array(count, ty) | Vector(count, ty) => ty.byte_size() * count,
            Pointer(_ty) => std::mem::size_of::<usize>(),
            Function(_, _, _) => panic!("Function type has no size"),
        }
    }
}

pub trait Typed<T: Type>: Sized {
    fn typify(&self) -> T;

    fn typify_list(values: &Vec<Self>) -> Vec<T> {
        values.into_iter().map(|value| value.typify()).collect()
    }

    fn typify_same(values: &Vec<Self>) -> T {
        if values.is_empty() {
            panic!("Empty arrays cannot be typed")
        }
        let mut values = values.into_iter();
        let result = values
            .next()
            .expect("Empty arrays cannot be typed")
            .typify();
        for value in values {
            if result != value.typify() {
                panic!("All types have to be the same");
            }
        }
        result
    }
}

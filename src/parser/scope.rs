use crate::{
    scope::{Definition, Scope},
    types::MachineType,
};
use token_lists::Token;
use token_parser::{Parsable, Parser, Result as ParseResult, Unit};

enum TopLevel {
    Library(String),
    Type { id: String, ty: MachineType },
    Definition { id: String, definition: Definition },
    Include(String),
}

impl Scope {
    pub fn parse_from_file(&mut self, filename: &str, file_parser: &impl Fn(&str) -> Vec<Token>) {
        let tokens = file_parser(filename);
        self.parse(tokens, file_parser);
    }

    pub fn parse(&mut self, tokens: Vec<Token>, file_parser: &impl Fn(&str) -> Vec<Token>) {
        let forms: Vec<_> = Parser::new(tokens.into_iter())
            .parse_rest(self)
            .expect("Parsing failed");
        for form in forms {
            self.apply(form, file_parser)
        }
    }

    fn apply(&mut self, toplevel: TopLevel, file_parser: &impl Fn(&str) -> Vec<Token>) {
        use TopLevel::*;
        match toplevel {
            Library(path) => self.libs.push(path),
            Type { id, ty } => {
                if self.types.contains_key(&id) {
                    panic!("Type named {} already defind", id);
                }
                self.types.insert(id, ty);
            }
            Definition { id, definition } => {
                if self.values.contains_key(&id) {
                    panic!("Value named {} already defind", id);
                }
                self.values.insert(id, definition);
            }
            Include(path) => self.parse_from_file(&path[..], file_parser),
        }
    }
}

impl Parsable<Scope> for TopLevel {
    fn parse_list<I: Iterator>(parser: &mut Parser<I>, scope: &Scope) -> ParseResult<Self>
    where
        I::Item: Into<Unit<I>>,
    {
        Ok(match &parser.parse_next::<_, String>(scope)?[..] {
            "lib" => Self::Library(parser.parse_next(scope)?),
            "type" => {
                let id = parser.parse_next(scope)?;
                let ty = parser.parse_next(scope)?;
                Self::Type { id, ty }
            }
            "val" => {
                let id = parser.parse_next(scope)?;
                let val = parser.parse_next(scope)?;
                Self::Definition {
                    id,
                    definition: Definition::Value(val),
                }
            }
            "fun" => {
                let id = parser.parse_next(scope)?;
                let val = parser.parse_rest(scope)?;
                Self::Definition {
                    id,
                    definition: Definition::Function(val),
                }
            }
            "ext" => {
                let id = parser.parse_next(scope)?;
                let ret = parser.parse_next(scope)?;
                let args = parser.parse_rest(scope)?;
                Self::Definition {
                    id,
                    definition: Definition::Extern(ret, args, None),
                }
            }
            "ext..." => {
                let id = parser.parse_next(scope)?;
                let ret = parser.parse_next(scope)?;
                let mut args: Vec<_> = parser.parse_rest(scope)?;
                let variadic = args.pop();
                Self::Definition {
                    id,
                    definition: Definition::Extern(ret, args, variadic),
                }
            }
            "inc" => Self::Include(parser.parse_next::<_, String>(scope)?),
            form => panic!("Form {} is not allowed at toplevel", form),
        })
    }
}

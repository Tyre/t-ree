use crate::{
    expressions::Expression,
    scope::Scope,
    values::{FloatValue, IntValue, PrimitiveValue},
};
use token_parser::{Error as ParseError, Parsable, Parser, Result as ParseResult, Unit};

fn parse_load(name: &str, scope: &Scope) -> Option<Expression> {
    let ty = scope.values.get(&name[..])?.typify();
    Some(Expression::Global(ty, name.to_string()))
}

fn parse_call<I: Iterator>(
    expression: Expression,
    parser: &mut Parser<I>,
    scope: &Scope,
) -> ParseResult<Expression>
where
    I::Item: Into<Unit<I>>,
{
    Ok(Expression::Call(
        Box::new(expression),
        parser.parse_rest(scope)?,
    ))
}

impl Parsable<Scope> for Expression {
    fn parse_symbol(name: String, scope: &Scope) -> ParseResult<Self> {
        Ok(parse_load(&name[..], scope).expect(&format!("Definition `{}` not found", name)[..]))
    }

    fn parse_list<I: Iterator>(parser: &mut Parser<I>, scope: &Scope) -> ParseResult<Self>
    where
        I::Item: Into<Unit<I>>,
    {
        let kind = &parser.parse_next::<_, String>(scope)?[..];
        if let Some(result) = parse_load(kind, scope) {
            return parse_call(result, parser, scope);
        }
        Ok(match kind {
            "b" => Self::Primitive(PrimitiveValue::Bool(
                match &parser.parse_next::<_, String>(scope)?[..] {
                    "true" | "1" => true,
                    "false" | "0" => false,
                    _ => return Err(ParseError::InvalidElement),
                },
            )),
            "i" => Expression::Primitive(PrimitiveValue::Int(IntValue::signed(
                parser.parse_next(scope)?,
                &parser.parse_next::<_, String>(scope)?[..],
            ))),
            "u" => Expression::Primitive(PrimitiveValue::Int(IntValue::unsigned(
                parser.parse_next(scope)?,
                &parser.parse_next::<_, String>(scope)?[..],
            ))),
            "f" => Expression::Primitive(PrimitiveValue::Float(FloatValue::new(
                parser.parse_next(scope)?,
                &parser.parse_next::<_, String>(scope)?[..],
            ))),

            "tuple" => Self::Tuple(parser.parse_rest(scope)?),
            "array" => Self::Array(parser.parse_rest(scope)?),
            "string" => Self::Array(
                parser
                    .parse_next::<_, String>(scope)?
                    .bytes()
                    .map(|value| Self::Primitive(PrimitiveValue::Int(IntValue::U8(value))))
                    .chain(Some(Self::Primitive(PrimitiveValue::Int(IntValue::U8(0)))))
                    .collect(),
            ),
            "vector" => Self::Vector(parser.parse_rest(scope)?),
            "p" => Self::Pointer(parser.parse_next(scope)?),
            "print" => Self::Print(parser.parse_rest(scope)?),
            "call" => parse_call(parser.parse_next(scope)?, parser, scope)?,
            "block" => Self::Block(parser.parse_rest(scope)?),

            "bitcast" => Self::Bitcast(parser.parse_next(scope)?, parser.parse_next(scope)?),
            "store" => Self::Store(parser.parse_next(scope)?, parser.parse_next(scope)?),
            "load" => Self::Load(parser.parse_next(scope)?),
            "the" => Self::Argument(parser.parse_next(scope)?, parser.parse_next(scope)?),
            _ => return Err(ParseError::InvalidElement),
        })
    }
}

use crate::{
    scope::Scope,
    types::{FloatType, IntType, MachineType, PrimitiveType},
};
use token_parser::{Error as ParseError, Parsable, Parser, Result as ParseResult, Unit};

fn parse_integer(size: u8, signed: bool) -> ParseResult<MachineType> {
    if let Some(integer) = IntType::new(size, signed) {
        Ok(MachineType::Primitive(PrimitiveType::Int(integer)))
    } else {
        Err(ParseError::InvalidElement)
    }
}

fn parse_float(size: u8) -> ParseResult<MachineType> {
    if let Some(float) = FloatType::new(size) {
        Ok(MachineType::Primitive(PrimitiveType::Float(float)))
    } else {
        Err(ParseError::InvalidElement)
    }
}

impl Parsable<Scope> for MachineType {
    fn parse_symbol(name: String, scope: &Scope) -> ParseResult<Self> {
        Ok(scope
            .types
            .get(&name[..])
            .expect(&format!("Type definition {} not found", name)[..])
            .clone())
    }

    fn parse_list<I: Iterator>(parser: &mut Parser<I>, scope: &Scope) -> ParseResult<Self>
    where
        I::Item: Into<Unit<I>>,
    {
        let kind = &parser.parse_next::<_, String>(scope)?[..];
        Ok(match kind {
            "b" => Self::Primitive(PrimitiveType::Bool),
            "i" => parse_integer(parser.parse_next(scope)?, true)?,
            "u" => parse_integer(parser.parse_next(scope)?, false)?,
            "f" => parse_float(parser.parse_next(scope)?)?,

            "tuple" => Self::Tuple(parser.parse_rest(scope)?),
            "array" => Self::Array(parser.parse_next(scope)?, parser.parse_next(scope)?),
            "vector" => Self::Vector(parser.parse_next(scope)?, parser.parse_next(scope)?),
            "p" => Self::Pointer(parser.parse_next(scope)?),
            "fun" => Self::Function(
                parser.parse_next(scope)?,
                parser.parse_rest(scope)?,
                Box::new(None),
            ),

            _ => return Err(ParseError::InvalidElement),
        })
    }
}

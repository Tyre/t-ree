use crate::{
    eval::EvalContext,
    scope::Scope,
    types::*,
    values::{MachineValue, PrimitiveValue, Value as _},
};

#[derive(Clone, Debug)]
pub enum Expression {
    // Values
    Primitive(PrimitiveValue),
    Tuple(Vec<Expression>),
    Array(Vec<Expression>),
    Vector(Vec<Expression>),
    Pointer(Box<Expression>),

    Print(Vec<Expression>),

    // Functions
    /*
    Return(Box<Expression>),
    AggregateReturn(Vec<Expression>),
    */
    Call(Box<Expression>, Vec<Expression>),

    // Memory
    /*
    Alloca,
    ArrayAlloca,
    Malloc,
    ArrayMalloc,
    Free,
    Memcpy,
    Memmove,
    */
    Store(Box<Expression>, Box<Expression>),
    Load(Box<Expression>),

    // Math
    /*
    Add(Vec<Expression>),
    Sub(Box<Expression>, Box<Expression>),
    Neg(Box<Expression>),

    Mul(Vec<Expression>),
    Div(Box<Expression>, Box<Expression>),
    Rem(Box<Expression>, Box<Expression>),

    Compare(Box<Expression>, Box<Expression>),
    */


    // Bit operations
    /*
    Not(Box<Expression>),
    Xor(Vec<Expression>),
    And(Vec<Expression>),
    Or(Vec<Expression>),
    LeftShift(Box<Expression>, Box<Expression>),
    RightShift(Box<Expression>, Box<Expression>),
    */
    // Memory layout
    Bitcast(Box<Expression>, MachineType),

    // TODO
    /*
    Gep,
    InBoundsGep,
    StructGep,
    PtrDiff,
    Phi,
    IntSExtend,
    AddressSpaceCast,
    IntSExtendOrBitCast,
    IntZExtend,
    IntZExtendOrBitCast,
    IntTruncate,
    IntTruncateOrBitCast,
    FloatToUnsignedInt,
    FloatToSignedInt,
    UnsignedIntToFloat,
    SignedIntToFloat,
    FloatTrunc,
    FloatExt,
    FloatCast,
    IntCast,
    Cast,
    PointerCast,
    UnconditionalBranch,
    ConditionalBranch,
    IndirectBranch,
    ExtractValue,
    InsertValue,
    ExtractElement,
    InsertElement,
    Unreachable,
    Fence,
    IsNull,
    IsNotNull,
    IntToPtr,
    PtrToInt,
    Switch,
    Select,
    GlobalString,
    GlobalStringPtr,
    ShuffleVector,
    VaArg,
    AtomicRMW,
    Cmpxchg,
    */
    // Extern
    Block(Vec<Expression>),

    Global(MachineType, String),
    Argument(MachineType, u32),
}

impl Expression {
    pub fn typify_body(expressions: &Vec<Expression>) -> MachineType {
        if let Some(expr) = expressions.last() {
            expr.typify() //TODO: might be more complicated
        } else {
            MachineType::Tuple(Vec::new())
        }
    }

    pub fn query_argument_types_list(
        expressions: &Vec<Expression>,
        types: &mut Vec<Option<MachineType>>,
    ) {
        for expression in expressions {
            expression.query_argument_types(types);
        }
    }

    pub fn query_argument_types(&self, types: &mut Vec<Option<MachineType>>) {
        use Expression::*;
        match self {
            Primitive(_) => (),
            Tuple(_) => (),
            Array(_) => (),
            Vector(_) => (),
            Pointer(val) => val.query_argument_types(types),
            Print(expressions) => Expression::query_argument_types_list(expressions, types),
            Call(_, expressions) => Expression::query_argument_types_list(expressions, types),
            Block(expressions) => Expression::query_argument_types_list(expressions, types),
            Bitcast(val, _ty) => val.query_argument_types(types),
            Store(ptr, val) => {
                ptr.query_argument_types(types);
                val.query_argument_types(types);
            }
            Load(ptr) => ptr.query_argument_types(types),
            Global(_, _) => (),
            Argument(ty, index) => {
                while types.len() as u32 <= *index {
                    types.push(None);
                }
                if types[*index as usize].is_none() {
                    types[*index as usize] = Some(ty.clone())
                }
            }
        }
    }

    pub fn eval_list(
        expressions: &Vec<Self>,
        scope: &Scope,
        context: &mut EvalContext,
    ) -> Vec<MachineValue> {
        expressions
            .into_iter()
            .map(|expression| expression.eval(scope, context))
            .collect()
    }

    pub fn eval_body(
        expressions: &Vec<Self>,
        scope: &Scope,
        context: &mut EvalContext,
    ) -> MachineValue {
        let mut expressions = expressions.iter();
        if let Some(last) = expressions.next_back() {
            for expression in expressions {
                expression.eval(scope, context);
            }
            last.eval(scope, context)
        } else {
            MachineValue::Tuple(Vec::new())
        }
    }

    pub fn eval(&self, scope: &Scope, context: &mut EvalContext) -> MachineValue {
        use Expression::*;
        match self {
            &Primitive(value) => MachineValue::Primitive(value),
            Tuple(expressions) => MachineValue::Tuple(Self::eval_list(expressions, scope, context)),
            Array(expressions) => MachineValue::Array(Self::eval_list(expressions, scope, context)),
            Vector(expressions) => {
                MachineValue::Vector(Self::eval_list(expressions, scope, context))
            }
            Pointer(expression) => {
                let value = expression.eval(scope, context);
                MachineValue::Pointer(context.allocaof(value))
            }
            Print(expressions) => {
                let mut expressions = expressions.into_iter();
                if let Some(expression) = expressions.next() {
                    print!("{}", expression.eval(scope, context).to_string());
                    for expression in expressions {
                        print!(" {}", expression.eval(scope, context).to_string());
                    }
                }
                println!();
                MachineValue::Tuple(Vec::new())
            }
            Call(fun, args) => {
                let fun = fun.eval(scope, context);
                let args = Self::eval_list(args, scope, context);
                fun.call(scope, context.call(args))
            }
            Block(body) => Self::eval_body(body, scope, context),
            Bitcast(expression, ty) => expression
                .eval(scope, context)
                .bitcast(ty)
                .expect("Bitcast failed"),
            Store(_ptr, _val) => unimplemented!(),
            Load(_ptr) => unimplemented!(),
            Global(_ty, name) => scope
                .values
                .get(name)
                .expect(&format!("Variable '{}' not defined", name)[..])
                .eval(scope, context),
            Argument(_ty, index) => context.args[*index as usize].clone(),
        }
    }
}

impl Typed<MachineType> for Expression {
    fn typify(&self) -> MachineType {
        use Expression::*;
        match self {
            Primitive(value) => MachineType::Primitive(value.typify()),
            Tuple(expressions) => MachineType::Tuple(Self::typify_list(expressions)),
            Array(expressions) => {
                MachineType::Array(expressions.len(), Box::new(Self::typify_same(expressions)))
            }
            Vector(expressions) => {
                MachineType::Vector(expressions.len(), Box::new(Self::typify_same(expressions)))
            }
            Pointer(expression) => MachineType::Pointer(Box::new(expression.typify())),

            Print(expressions) => MachineType::Tuple(Vec::new()),

            Call(fun, _args) => {
                fn typify_call(ty: MachineType) -> Box<MachineType> {
                    match ty {
                        MachineType::Function(ty, _args, _variadic) => ty,
                        MachineType::Pointer(ty) => typify_call(*ty),
                        _ => panic!("Uncallable type {:?}", ty),
                    }
                }
                *typify_call(fun.typify())
            }
            Block(expressions) => Expression::typify_body(expressions),
            Bitcast(_val, ty) => ty.clone(),
            Store(_ptr, _val) => MachineType::Tuple(Vec::new()),
            Load(ptr) => {
                if let MachineType::Pointer(ty) = ptr.typify() {
                    *ty
                } else {
                    panic!("Only pointers can be loaded")
                }
            }
            Global(ty, _) => ty.clone(),
            Argument(ty, _) => ty.clone(),
        }
    }
}

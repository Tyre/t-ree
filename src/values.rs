use crate::{eval::EvalContext, expressions::Expression, scope::Scope, types::*};
use std::{
    convert::TryInto,
    fmt::{Display, Formatter, Result as DisplayResult},
    mem::transmute,
};

pub trait Value: Sized {
    type Type: Type;
    fn to_bytes(&self) -> Vec<u8>;
    fn from_bytes(ty: &Self::Type, bytes: &[u8]) -> Option<Self>;

    fn bitcast(&self, ty: &Self::Type) -> Option<Self> {
        Self::from_bytes(ty, &self.to_bytes()[..])
    }
}

fn sequence_to_bytes<'a>(values: impl IntoIterator<Item = &'a (impl Value + 'a)>) -> Vec<u8> {
    values.into_iter().fold(Vec::new(), |mut result, value| {
        result.append(&mut value.to_bytes());
        result
    })
}

fn bytes_to_sequence<'a, V: Value>(
    types: impl IntoIterator<Item = &'a V::Type>,
    bytes: &[u8],
) -> Option<Vec<V>>
where
    V::Type: 'a,
{
    let mut offset = 0;
    types
        .into_iter()
        .map(|ty| {
            let last_offset = offset;
            offset += ty.byte_size();
            Value::from_bytes(ty, &bytes[last_offset..offset])
        })
        .collect()
}

#[derive(Copy, Clone, Debug)]
pub enum IntValue {
    I8(i8),
    I16(i16),
    I32(i32),
    I64(i64),
    I128(i128),
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
    U128(u128),
}

impl IntValue {
    pub fn signed(size: u8, number: &str) -> Self {
        use IntValue::*;
        match size {
            8 => I8(number.parse().expect("Integer type not parsable")),
            16 => I16(number.parse().expect("Integer type not parsable")),
            32 => I32(number.parse().expect("Integer type not parsable")),
            64 => I64(number.parse().expect("Integer type not parsable")),
            128 => I128(number.parse().expect("Integer type not parsable")),
            size => panic!("Signed integers of size {} not supported", size),
        }
    }

    pub fn unsigned(size: u8, number: &str) -> Self {
        use IntValue::*;
        match size {
            8 => I8(number.parse().expect("Integer type not parsable")),
            16 => I16(number.parse().expect("Integer type not parsable")),
            32 => I32(number.parse().expect("Integer type not parsable")),
            64 => I64(number.parse().expect("Integer type not parsable")),
            128 => I128(number.parse().expect("Integer type not parsable")),
            size => panic!("Unsigned integers of size {} not supported", size),
        }
    }

    pub fn new(size: u8, signed: bool, number: &str) -> Self {
        if signed {
            Self::signed(size, number)
        } else {
            Self::unsigned(size, number)
        }
    }
}

impl Display for IntValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> DisplayResult {
        use IntValue::*;
        match self {
            I8(value) => write!(f, "{}", value),
            I16(value) => write!(f, "{}", value),
            I32(value) => write!(f, "{}", value),
            I64(value) => write!(f, "{}", value),
            I128(value) => write!(f, "{}", value),

            U8(value) => write!(f, "{}", value),
            U16(value) => write!(f, "{}", value),
            U32(value) => write!(f, "{}", value),
            U64(value) => write!(f, "{}", value),
            U128(value) => write!(f, "{}", value),
        }
    }
}

impl Value for IntValue {
    type Type = IntType;
    fn to_bytes(&self) -> Vec<u8> {
        use IntValue::*;
        match self {
            I8(number) => number.to_ne_bytes().to_vec(),
            I16(number) => number.to_ne_bytes().to_vec(),
            I32(number) => number.to_ne_bytes().to_vec(),
            I64(number) => number.to_ne_bytes().to_vec(),
            I128(number) => number.to_ne_bytes().to_vec(),

            U8(number) => number.to_ne_bytes().to_vec(),
            U16(number) => number.to_ne_bytes().to_vec(),
            U32(number) => number.to_ne_bytes().to_vec(),
            U64(number) => number.to_ne_bytes().to_vec(),
            U128(number) => number.to_ne_bytes().to_vec(),
        }
    }

    fn from_bytes(ty: &IntType, bytes: &[u8]) -> Option<Self> {
        use IntType::*;
        Some(match ty {
            I8 => Self::I8(i8::from_ne_bytes(bytes.try_into().ok()?)),
            I16 => Self::I16(i16::from_ne_bytes(bytes.try_into().ok()?)),
            I32 => Self::I32(i32::from_ne_bytes(bytes.try_into().ok()?)),
            I64 => Self::I64(i64::from_ne_bytes(bytes.try_into().ok()?)),
            I128 => Self::I128(i128::from_ne_bytes(bytes.try_into().ok()?)),
            U8 => Self::U8(u8::from_ne_bytes(bytes.try_into().ok()?)),
            U16 => Self::U16(u16::from_ne_bytes(bytes.try_into().ok()?)),
            U32 => Self::U32(u32::from_ne_bytes(bytes.try_into().ok()?)),
            U64 => Self::U64(u64::from_ne_bytes(bytes.try_into().ok()?)),
            U128 => Self::U128(u128::from_ne_bytes(bytes.try_into().ok()?)),
        })
    }
}

impl Typed<IntType> for IntValue {
    fn typify(&self) -> IntType {
        use IntValue::*;
        match self {
            I8(_) => IntType::I8,
            I16(_) => IntType::I16,
            I32(_) => IntType::I32,
            I64(_) => IntType::I64,
            I128(_) => IntType::I128,

            U8(_) => IntType::U8,
            U16(_) => IntType::U16,
            U32(_) => IntType::U32,
            U64(_) => IntType::U64,
            U128(_) => IntType::U128,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum FloatValue {
    F32(f32),
    F64(f64),
}

impl FloatValue {
    pub fn new(size: u8, number: &str) -> Self {
        use FloatValue::*;
        match size {
            32 => F32(number.parse().expect("Float type not parsable")),
            64 => F64(number.parse().expect("Float type not parsable")),
            size => panic!("Floats of size {} not supported", size),
        }
    }
}

impl Display for FloatValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> DisplayResult {
        use FloatValue::*;
        match self {
            F32(value) => write!(f, "{}", value),
            F64(value) => write!(f, "{}", value),
        }
    }
}

impl Value for FloatValue {
    type Type = FloatType;
    fn to_bytes(&self) -> Vec<u8> {
        use FloatValue::*;
        match self {
            F32(number) => number.to_ne_bytes().to_vec(),
            F64(number) => number.to_ne_bytes().to_vec(),
        }
    }

    fn from_bytes(ty: &Self::Type, bytes: &[u8]) -> Option<Self> {
        use FloatType::*;
        Some(match ty {
            F32 => Self::F32(f32::from_ne_bytes(bytes.try_into().ok()?)),
            F64 => Self::F64(f64::from_ne_bytes(bytes.try_into().ok()?)),
        })
    }
}

impl Typed<FloatType> for FloatValue {
    fn typify(&self) -> FloatType {
        use FloatValue::*;
        match self {
            F32(_) => FloatType::F32,
            F64(_) => FloatType::F64,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum PrimitiveValue {
    Bool(bool),
    Int(IntValue),
    Float(FloatValue),
}

impl Display for PrimitiveValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> DisplayResult {
        use PrimitiveValue::*;
        match self {
            Bool(value) => write!(f, "{}", if *value { "true" } else { "false" }),
            Int(value) => write!(f, "{}", value.to_string()),
            Float(value) => write!(f, "{}", value.to_string()),
        }
    }
}

impl Value for PrimitiveValue {
    type Type = PrimitiveType;
    fn to_bytes(&self) -> Vec<u8> {
        use PrimitiveValue::*;
        match self {
            &Bool(value) => vec![if value { 1 } else { 0 }],
            Int(value) => value.to_bytes(),
            Float(value) => value.to_bytes(),
        }
    }

    fn from_bytes(ty: &Self::Type, bytes: &[u8]) -> Option<Self> {
        use PrimitiveType::*;
        Some(match ty {
            Bool => PrimitiveValue::Bool(bytes.get(0)? & 1 == 1),
            Int(ty) => PrimitiveValue::Int(IntValue::from_bytes(ty, bytes)?),
            Float(ty) => PrimitiveValue::Float(FloatValue::from_bytes(ty, bytes)?),
        })
    }
}

impl Typed<PrimitiveType> for PrimitiveValue {
    fn typify(&self) -> PrimitiveType {
        use PrimitiveValue::*;
        match self {
            Bool(_) => PrimitiveType::Bool,
            Int(value) => PrimitiveType::Int(value.typify()),
            Float(value) => PrimitiveType::Float(value.typify()),
        }
    }
}

#[derive(Clone, Debug)]
pub enum MachineValue {
    Primitive(PrimitiveValue),
    Tuple(Vec<MachineValue>),
    Array(Vec<MachineValue>),
    Vector(Vec<MachineValue>),
    Pointer(*mut MachineValue),
    Function(Vec<Expression>),
    Extern(MachineType, Vec<MachineType>, Option<MachineType>),
}

impl MachineValue {
    pub fn call(&self, scope: &Scope, mut context: EvalContext) -> MachineValue {
        match self {
            Self::Function(expressions) => Expression::eval_body(expressions, scope, &mut context),
            _ => panic!("Only function values are currently callable"),
        }
    }

    pub fn list_string(values: &Vec<Self>) -> String {
        let mut result = String::new();
        let mut values = values.into_iter();
        if let Some(first) = values.next() {
            result.push_str(&first.to_string()[..]);
            for value in values {
                result.push_str(" ");
                result.push_str(&value.to_string()[..]);
            }
        }
        result
    }
}

impl Display for MachineValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> DisplayResult {
        use MachineValue::*;
        match self {
            Primitive(value) => write!(f, "{}", value.to_string()),
            Tuple(values) => write!(f, "({})", Self::list_string(values)),
            Array(values) => {
                if let Some(values) = values
                    .into_iter()
                    .map(|value| {
                        if let MachineValue::Primitive(PrimitiveValue::Int(IntValue::U8(value))) =
                            value
                        {
                            Some(*value)
                        } else {
                            None
                        }
                    })
                    .collect::<Option<Vec<_>>>()
                {
                    write!(
                        f,
                        "{}",
                        std::str::from_utf8(&values[..]).expect("Printing string failed")
                    )
                } else {
                    write!(f, "{{{}}}", Self::list_string(values))
                }
            }
            Vector(values) => write!(f, "[{}]", Self::list_string(values)),
            Pointer(p) => write!(f, "{:x}", unsafe { std::mem::transmute::<_, usize>(p) }),
            Function(body) => write!(f, "<Function>"),
            Extern(_, _, _) => unimplemented!(),
        }
    }
}

impl Value for MachineValue {
    type Type = MachineType;
    fn to_bytes(&self) -> Vec<u8> {
        use MachineValue::*;
        match self {
            Primitive(value) => value.to_bytes(),
            Tuple(values) | Array(values) | Vector(values) => sequence_to_bytes(values),
            Pointer(value) => {
                unsafe { transmute::<_, [u8; std::mem::size_of::<usize>()]>(value) }.to_vec()
            }
            Function(_) | Extern(_, _, _) => {
                panic!("Byte representations for function or extern do not exist")
            }
        }
    }

    fn from_bytes(ty: &Self::Type, bytes: &[u8]) -> Option<Self> {
        use MachineType::*;
        Some(match ty {
            Primitive(ty) => MachineValue::Primitive(Value::from_bytes(ty, bytes)?),
            Tuple(types) => MachineValue::Tuple(bytes_to_sequence(types, bytes)?),
            Array(count, ty) => MachineValue::Array(bytes_to_sequence(
                std::iter::repeat(&**ty).take(*count),
                bytes,
            )?),
            Vector(count, ty) => MachineValue::Vector(bytes_to_sequence(
                std::iter::repeat(&**ty).take(*count),
                bytes,
            )?),
            Pointer(_ty) => MachineValue::Pointer(unsafe {
                transmute::<[u8; std::mem::size_of::<usize>()], *mut MachineValue>(
                    bytes.try_into().ok()?,
                )
            }),
            Function(_, _, _) => return None,
        })
    }
}

fn make_function_type(
    expressions: &Vec<Expression>,
) -> (Box<MachineType>, Vec<MachineType>, Box<Option<MachineType>>) {
    let mut argument_types = Vec::new();
    Expression::query_argument_types_list(expressions, &mut argument_types);
    (
        Box::new(Expression::typify_body(expressions)),
        argument_types
            .into_iter()
            .map(|argument_type| {
                if let Some(ty) = argument_type {
                    ty
                } else {
                    MachineType::Tuple(Vec::new())
                }
            })
            .collect(),
        Box::new(None),
    )
}

impl Typed<MachineType> for MachineValue {
    fn typify(&self) -> MachineType {
        use MachineValue::*;
        match self {
            Primitive(value) => MachineType::Primitive(value.typify()),
            Tuple(values) => MachineType::Tuple(Self::typify_list(values)),
            Array(values) => MachineType::Array(values.len(), Box::new(Self::typify_same(values))),
            Vector(values) => {
                MachineType::Vector(values.len(), Box::new(Self::typify_same(values)))
            }
            Pointer(values) => MachineType::Pointer(Box::new(unsafe { &**values }.typify())),
            Function(expressions) => {
                let (ret, args, variadic) = make_function_type(expressions);
                MachineType::Function(ret, args, variadic)
            }
            Extern(ret, args, variadic) => MachineType::Function(
                Box::new(ret.clone()),
                args.clone(),
                Box::new(variadic.clone()),
            ),
        }
    }
}

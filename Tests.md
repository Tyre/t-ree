# Functions

* function call
* function call with argument
* function call with multiple arguments
* function pointer call
* extern call
* automatic return
* manual return
* multi value return

# Values

* Global values
* All types

# Math

Tests for all Number types:

* Sum
* Difference
* Negation
* Multiplicaiton
* Division
* Remainder

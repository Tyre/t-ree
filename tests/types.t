fun' some-bool' (b: 1)
fun' some-unsigned' (u'32:1)
fun' some-signed' (i'32:1)
fun' some-float' (f'32:1)
#fun' some-tuple' ((tuple b:1 u'32:1 i'32:1 f'32:1))
fun' some-array' ((array f'32:1 f'32:1 f'32:1))
fun' some-vector' ((vector f'32:1 f'32:1 f'32:1))
fun' some-pointer' ((p (i 32 1)))

fun' types' (
  some-bool '()
  some-unsigned' ()
  some-signed' ()
  some-float' ()
  #some-tuple' ()
  some-array' ()
  some-vector' ()
  some-pointer' ()
  i'32:0
)


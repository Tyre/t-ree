fun' call1'(
    (printf
        (bitcast p:"Test %i\n" p:u8) (i 32 1))
        (i 32 0))

fun' call2' (
    (printf
        (bitcast p:"Test %i\n" p:u8) (the i32 0))
        (i 32 0))

fun' call3'(
    (printf
        (bitcast p:"Test %s %i %f\n" p: u8) (the p:u8 2) (the i32 0) (the f64 1))
        (i 32 0))

fun' call4: i'32:4

fun' calls'(
  call1'()
  call2: (i 32 2)
  (call3 (i 32 3) (f 64 3) (bitcast p:"3!" p: u8))
  printf'(
    (bitcast p:"Test %i\n" p:u8)
    (call4))
  (i 32 0))

